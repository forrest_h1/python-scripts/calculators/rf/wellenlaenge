# Wellenlänge



Wellen(länge) is a very, very simple utility to calculate the wavelength of a given frequency in Hz(Hz,Khz,Mhz,Ghz) to a length in Meters (Meters,Millimeters,Centimeters) in a terminal. Truthfully, I actually just wrote this to have it on my phone's Termux instance rather than to impress anyone. We are going based off of this equation:



```

λ = ( (∁ / 𝐟) * 𝐦 )


λ = Total Wavelength of a given frequency
∁ = Speed of Light (3.0 * 10^8)
𝐟 = Frequency in Hz
𝐦  = Meter Conversion (Meter = 1, Cm = 100, etc.)

```


The real reason this exists is because of my obsession with creating the perfect overly-designed makeshift WiFi Cantenna for 2.4Ghz and 5Ghz. I have no interest in math outside of this, and that is precisely why it exists (Because I tend to make better end-product if a computer is calculating everything for me). There are probably some better ways to handle the math, but for now, it's pretty accurately given me the correct wavelength for every weird thing I throw at it.



There are no real requirements other than `argparse`. Everything else used is a built-in.



## Usage



After cloning the repo and running `chmod u+x` on `wellen.py`, you'll need to give it a frequency in some variation of Hz. For example, if we wanted to calculate the wavelength of 2.4Ghz, the base frequency most 802.11 transmissions occur on, we would simply enter `./wellen.py -f 2.4Ghz`. The argument **MUST** have the Hertz designator within the same substring (Though I'm working on removing this requirement). This will return the following output:



```
$ ./wellen.py -f 2.4Ghz

Wellenlaenge v0.4 - https://gitlab.com/forrest_h1/python-scripts/calculators/rf/wellenlaenge

Unit not given. Defaulting to 'Cm' for output.

Given Frequency: 2.4GHZ
Calculated Wavelength: 12.5 CM

```



`-f` can accept both floating-points and integers for arguments, but be aware that `1000Hz` and `1000Ghz` are very, VERY different things.



You'll note that not supplying a unit will give us the default output unit of Centimeters - if we want to specify another unit (based on meters), we'll need to specify `-u`. This MUST be some form of metric - I will be adding imperial conversions later on down the road.



Example output for `-u`:


```
$ ./wellen.py -f 2.4Ghz -u mm

Wellenlaenge v0.4 - https://gitlab.com/forrest_h1/python-scripts/calculators/rf/wellenlaenge

Measurement Millimeters selected.

Given Frequency: 2.4GHZ
Calculated Wavelength: 125.0 MM

```


