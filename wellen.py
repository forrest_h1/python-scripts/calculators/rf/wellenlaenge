#!/usr/bin/python3
#
#Wellen - Calculator to find wavelength of a given frequency
#
#Written by Forrest Hooker, 05/03/2023

import os, sys, argparse


RESET = '\033[0m'
BOLD = '\033[1m'
ULINE = '\033[4m'
RED = '\033[31m'
CYAN = '\033[36m'
GREEN = '\033[32m'
YELLOW = '\033[33m'
WHITE = '\033[37m'
# Bold Colors #
B_U = ('{}{}'.format(BOLD,ULINE))
B_R = ('{}{}'.format(BOLD,RED))
B_C = ('{}{}'.format(BOLD,CYAN))
B_G = ('{}{}'.format(BOLD,GREEN))
B_Y = ('{}{}'.format(BOLD,YELLOW))
B_W = ('{}{}'.format(BOLD,WHITE))
# Bold Underline Colors #
B_U_R = ('{}{}'.format(B_U,RED))
B_U_G = ('{}{}'.format(B_U,GREEN))
B_U_Y = ('{}{}'.format(B_U,YELLOW))
B_U_W = ('{}{}'.format(B_U,WHITE))
# Reset To Color #
R_R = ('{}{}'.format(RESET,RED))
R_G = ('{}{}'.format(RESET,GREEN))
R_Y = ('{}{}'.format(RESET,YELLOW))
R_W = ('{}{}'.format(RESET,WHITE))

#Header for program
calcHeader = ("{}Wellenlaenge v0.4{} - https://gitlab.com/forrest_h1/python-scripts/calculators/rf/wellenlaenge{}".format(B_C,R_W,RESET))

#Speed of light in m/s
lightSpeed = (3.0 * (10 ** 8))

#Table of exponential conversions for user's specified frequency unit
hzTable = {
        "ghz": (10 ** 9),
        "mhz": (10 ** 6),
        "khz": (10 ** 3),
        "hz": (1)
        }

#Table of exponential conversions for user's specified output unit
meterTable = {
        "m": 1,
        "cm": 100,
        "mm": 1000
        }

#Table of unit output strings
userMeters = {
        "m": "Meters",
        "cm": "Centimeters",
        "mm": "Millimeters"
        }

def __argParse__():
    global userFreq, userUnit, unitConv, unitStr
    print("{}\n".format(calcHeader))
    #If no arguments given, notify user and exit.
    if len(sys.argv) == 1:
        print("{}Empty arguments.{}".format(B_R,RESET))
        sys.exit()
    else:
        parser = argparse.ArgumentParser()
        #Add argument to specify frequency
        parser.add_argument('-f', '--freq', '--frequency',
                dest='userFreq',
                help='Specify a frequency in Hz (Ghz, Mhz, Khz, Hz)'
                )
        #Add argument to specify output unit
        parser.add_argument('-u', '--unit',
                dest='userUnit',
                help='Specify a unit of measurement in M (M, MM, CM)'
                )
        #Set args 
        args = parser.parse_args()

        if args:
            userFreq = args.userFreq
            userUnit = args.userUnit

        ## Frequency Check ##
        
        #If no argument is given for userFreq, notify user and exit.
        if not userFreq:
            print("{}No frequency given!{}".format(termf.B_R,termf.RESET))
            sys.exit()
        else:
            __freqCheck__(userFreq)
        
        ## Output Unit Check ##

        if not userUnit:
            print("{}Unit not given. {}Defaulting to 'Cm' for output.\n{}".format(B_Y,R_W,RESET))
            unitConv = meterTable["cm"]
            unitStr = str("CM")
        else:
            #Set case-insensitive version of userUnit for matching
            unitCase = userUnit.lower()
            #for each unit within meterTable's keys...
            for meterUnit in meterTable.keys():
                #if meterUnit is a match for case-insen user's unit
                if meterUnit == unitCase:
                    #Set unitConv to the correct value
                    unitConv = int(meterTable[meterUnit])
                    #Set unitStr to the key string of the correct value
                    unitStr = str(meterUnit).upper()
                    print ("{}Measurement {}{}{} selected.{}\n".format(GREEN,B_U,userMeters[meterUnit],R_G,RESET))
                    break

        __calculateLength__()


def __freqCheck__(userFreq):
    global freqConv, freqString, userNum
    #Set case-insensitive version of userFreq
    freqCase = userFreq.lower()
    #For every possible unit in hzTable's keys...
    for freqUnit in hzTable.keys():
        #If the unit matches the case-insen version of userFreq...
        if freqUnit in freqCase:
            #Set freqConv to the value found in hzTable
            freqConv = hzTable[freqUnit]
            #Set freqString to the Upper version of freqUnit
            freqString = freqUnit.upper()
            #Set userNum to number only version of userFreq
            userNum = (freqCase.replace(freqUnit, ""))
            #If userNum is an integer, set userFreq accordingly and return
            if userNum.isdigit():
                userNum = int(userNum)
                return userFreq, freqConv, freqString
            #Else, try to set it as a float, and exit if an exception is thrown.
            else:
                try:
                    userNum = float(userNum)
                    return userFreq, freqConv, freqString
                except:
                    print("{}Invalid value given. {}Please try giving frequency in Hz, Khz, Mhz, or Ghz.{}".format(B_R,R_Y,RESET))
                    sys.exit()
    #If loop completes without finding a valid match, notify user and exit.
    print("{}Unit not found. {}Please try giving a frequency in Hz, Khz, Mhz, or Ghz.{}".format(B_R,R_Y,RESET))
    sys.exit()


#Function to actually calculate the wavelength given user's arguments
def __calculateLength__():
    waveLength = ((lightSpeed / (userNum * (freqConv)) * unitConv))
    print("{}Given Frequency: {}{}{}{}".format(B_W,R_W,userNum,freqString,RESET))
    print("{}Calculated Wavelength: {}{} {}{}\n".format(B_W,R_G,waveLength,unitStr,RESET)) 


__argParse__()


